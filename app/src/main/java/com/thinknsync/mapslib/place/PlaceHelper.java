package com.thinknsync.mapslib.place;


import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.positionmanager.TrackingData;

import java.util.List;

/**
 * Created by shuaib on 5/24/17.
 */

public interface PlaceHelper extends ObserverHost<List<PlaceDataObject>> {
    void guessCurrentPlace() throws SecurityException;
    void guessPlaceFromCoordiante(TrackingData trackingData);
    void getPlaceFromPlaceId(String placeId);
}
