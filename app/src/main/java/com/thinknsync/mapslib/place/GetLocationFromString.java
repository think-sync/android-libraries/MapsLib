package com.thinknsync.mapslib.place;

import com.thinknsync.observerhost.ObserverHost;

import java.util.List;

/**
 * Created by shuaib on 5/23/17.
 */

public interface GetLocationFromString extends LocationPickerHelper, ObserverHost<List<PlaceDataObject>> {
    void findPlace(String searchArg);
}
