package com.thinknsync.mapslib.place;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import androidx.annotation.NonNull;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.PlaceLikelihood;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest;
import com.google.android.libraries.places.api.net.FindCurrentPlaceResponse;
import com.thinknsync.logger.Logger;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.positionmanager.TrackingData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class GooglePlaceHelper extends GooglePlaceClientContainer implements PlaceHelper {

    private final List<Place.Field> placeProperties = Arrays.asList(Place.Field.ID, Place.Field.NAME,
            Place.Field.ADDRESS, Place.Field.LAT_LNG, Place.Field.TYPES);

    private Context context;

    public GooglePlaceHelper(AndroidContextWrapper contextWrapper, Logger logger) {
        super(contextWrapper, logger);
        this.context = contextWrapper.getFrameworkObject();
    }

    @Override
    public void guessCurrentPlace() throws SecurityException {
        FindCurrentPlaceRequest request = FindCurrentPlaceRequest.builder(placeProperties).build();
        placesClient.findCurrentPlace(request).addOnSuccessListener(new OnSuccessListener<FindCurrentPlaceResponse>() {
            @Override
            public void onSuccess(FindCurrentPlaceResponse findCurrentPlaceResponse) {
                List<PlaceDataObject> places = new ArrayList<>();
                for (PlaceLikelihood placeLikelihood : findCurrentPlaceResponse.getPlaceLikelihoods()){
                    places.add(getPlaceObjectFromGooglePlace(placeLikelihood.getPlace()));
                }
                notifyObservers(places);
            }
        }).addOnFailureListener(getOnFailureListener());
    }

    private PlaceDataObject getPlaceObjectFromGooglePlace(Place place) {
        logger.debugLog("my place (likely)", place.getLatLng().latitude + " " + place.getLatLng().longitude);

        PlaceDataObject placeDataObject = new GooglePlaceDataObject(place.getId(), place.getName(),
                place.getAddress());
        placeDataObject.setLat(place.getLatLng().latitude);
        placeDataObject.setLon(place.getLatLng().longitude);
        return placeDataObject;
    }

    @Override
    public void guessPlaceFromCoordiante(final TrackingData locationData) {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
                final Geocoder geocoder;
                geocoder = new Geocoder(context, Locale.getDefault());
                try {
                    List<Address> addresses = geocoder.getFromLocation(locationData.getLat(), locationData.getLon(), 1);
                    List<PlaceDataObject> places = getPlaceDataObjectsFromAddressList(addresses);
                    notifyObservers(places);
                } catch (IOException e) {
                    notifyError(e);
                }
//            }
//        }).start();
    }

    private List<PlaceDataObject> getPlaceDataObjectsFromAddressList(List<Address> addresses) {
        List<PlaceDataObject> placeDataObjectList = new ArrayList<>();

        for (final Address address : addresses) {
            PlaceDataObject placeDataObject = new GooglePlaceDataObject();
            placeDataObject.setPlaceTitle(address.getAddressLine(0));
            placeDataObject.setLat(address.getLatitude());
            placeDataObject.setLon(address.getLongitude());
            placeDataObjectList.add(placeDataObject);
        }

        return placeDataObjectList;
    }

    private OnFailureListener getOnFailureListener(){
        return new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                if (exception instanceof ApiException) {
                    ApiException apiException = (ApiException) exception;
                    logger.errorLog("Place not found: ", apiException.getMessage() +
                            ", status code: " + apiException.getStatusCode());
                }
            }
        };
    }


    @Override
    public void getPlaceFromPlaceId(String placeId) {
        FetchPlaceRequest request = FetchPlaceRequest.builder(placeId, placeProperties).build();
        placesClient.fetchPlace(request).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
            @Override
            public void onSuccess(final FetchPlaceResponse fetchPlaceResponse) {
                notifyObservers(new ArrayList<PlaceDataObject>(){{
                    add(getPlaceObjectFromGooglePlace(fetchPlaceResponse.getPlace()));
                }});
            }
        }).addOnFailureListener(getOnFailureListener());
    }
}
