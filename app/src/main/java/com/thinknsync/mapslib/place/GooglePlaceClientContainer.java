package com.thinknsync.mapslib.place;

import android.content.Context;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.thinknsync.logger.Logger;
import com.thinknsync.mapslib.Utils;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.ObserverHostImpl;

import java.util.Arrays;
import java.util.List;


public abstract class GooglePlaceClientContainer extends ObserverHostImpl<List<PlaceDataObject>> {

    protected static PlacesClient placesClient;
    protected Logger logger;
    protected final List<Place.Field> placeProperties = Arrays.asList(Place.Field.ID, Place.Field.NAME,
            Place.Field.ADDRESS, Place.Field.LAT_LNG, Place.Field.TYPES);

    public GooglePlaceClientContainer(AndroidContextWrapper contextWrapper, Logger logger) {
        Context context = contextWrapper.getFrameworkObject();
        if(placesClient == null) {
//            Places.initialize(context, "AIzaSyAgaVrB3qdWgchEjTvm8IYzHpwsClR2GOE");
            Places.initialize(context, Utils.getGoogleApiKey(contextWrapper));
            placesClient = Places.createClient(context);
        }
        this.logger = logger;
    }
}
