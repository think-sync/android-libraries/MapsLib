package com.thinknsync.mapslib.place;

/**
 * Created by shuaib on 5/19/17.
 */

public interface LocationPickerHelper {
    int PLACE_REQUEST_CODE = 1;
    int PLACE_PICKER_REQUEST_PLACE_PICKER = 2;
}
