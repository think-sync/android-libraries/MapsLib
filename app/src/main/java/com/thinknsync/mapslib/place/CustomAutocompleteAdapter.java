package com.thinknsync.mapslib.place;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.thinknsync.listviewadapterhelper.BaseListViewAdapter;
import com.thinknsync.mapslib.R;

import java.util.List;

/**
 * Created by shuaib on 3/2/18.
 */

public class CustomAutocompleteAdapter extends BaseListViewAdapter<PlaceDataObject> {

    public CustomAutocompleteAdapter(List<PlaceDataObject> resultPlaces, @NonNull Context context) {
        super(context, resultPlaces);
    }

    @Override
    protected void setValues(View convertView, PlaceDataObject item) {
        TextView place = convertView.findViewById(R.id.spinner_text);
        place.setText(item.getPlaceTitle());
//        TextView taskDate = convertView.findViewById(R.id.task_date);
//        taskDate.setText("Date: " + item.getDateString());
//        TextView taskTitle = convertView.findViewById(R.id.task_title);
//        taskTitle.setText("Purpose: " + item.getPurpose());
//        TextView taskPlace = convertView.findViewById(R.id.task_place);
//        taskPlace.setText("Place: " + item.getPlace());
//        TextView taskStatus = convertView.findViewById(R.id.task_status);
//        taskStatus.setText("Status: " + item.getStatus());
        setListener(convertView, item);

        
    }

    @Override
    protected int getView() {
        return R.layout.spinner_item_layout;
    }

    private void setListener(View view, final PlaceDataObject item) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyObservers(item);
            }
        });
    }
}
