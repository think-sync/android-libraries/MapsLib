package com.thinknsync.mapslib;

import android.annotation.SuppressLint;
import android.graphics.Color;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;
import com.thinknsync.mapslib.mapWorks.OnMapsReady;
import com.thinknsync.mapslib.mapWorks.mapListeners.InitMapLocationObserver;
import com.thinknsync.mapslib.mapWorks.markers.MarkerInformation;
import com.thinknsync.mapslib.mapsDrawer.GoogleMapsPathDrawer;
import com.thinknsync.mapslib.mapsDrawer.PathDataManager;
import com.thinknsync.mapslib.mapsDrawer.PathDrawer;
import com.thinknsync.mapslib.mapsDrawer.PathProperties;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.positionmanager.GpsPositionManager;
import com.thinknsync.positionmanager.LocationTracker;
import com.thinknsync.positionmanager.TrackingData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shuaib on 5/17/17.
 */

public class GoogleMapsCallback implements OnMapReadyCallback, MapsOperation<GoogleMap> {

    private GoogleMap googleMap;
    private OnMapsReady onMapsReady;
    private PathDrawer routePathDrawer;

    private ObserverHost<TrackingData> mapClickObserverHost;
    private ObserverHost mapDragObserverHost;
    private ObserverHost<MarkerInformation> mapMarkerClickObserverHost;
    private ObserverHost mapMoveStartObserverHost;
    private ObserverHost mapIdleObserverHost;

    private boolean isMapMoving = false;

    private float defaultZoomLevel = MapsOperation.defaultZoomLevel;
    private AndroidContextWrapper contextWrapper;
    private float maxZoomLevel = MapsOperation.maxZoomLevel;
    private float minZoomLevel = MapsOperation.minZoomLevel;
    private boolean shouldGoToCurrentLocation = MapsOperation.goToCurrentLocationOnMapStart;

    public GoogleMapsCallback(AndroidContextWrapper contextWrapper, OnMapsReady mapsReady) {
        this.contextWrapper = contextWrapper;
        onMapsReady = mapsReady;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        setupDefaultGoogleMaps();
        routePathDrawer = new GoogleMapsPathDrawer(googleMap, contextWrapper.getFrameworkObject());
        onMapsReady.onMapsReady();
        if(shouldGoToCurrentLocation) {
            initMapView();
        }
        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                onMapsReady.onMapsLoadComplete();
            }
        });
    }

    private void initMapView() {
        final TypedObserver<TrackingData> mapsInitObserver = new InitMapLocationObserver(this);
        GpsPositionManager positionManager = LocationTracker.getInstance(contextWrapper);
        positionManager.addToObservers(mapsInitObserver);
        positionManager.startTracking();
    }

    @Override
    public void setShouldGoToCurrentLocation(boolean shouldGoToCurrentLocation) {
        this.shouldGoToCurrentLocation = shouldGoToCurrentLocation;
    }

    // location permission available in locationLib
    @SuppressLint("MissingPermission")
    private void setupDefaultGoogleMaps() {
        try {
            googleMap.setMyLocationEnabled(true);
            googleMap.setPadding(0, 150, 0, 0);
        } catch (SecurityException r) {
            r.printStackTrace();
        }
        googleMap.setMaxZoomPreference(maxZoomLevel);
        googleMap.setMinZoomPreference(minZoomLevel);
    }

    @Override
    public float getDefaultZoomLevel() {
        return defaultZoomLevel;
    }

    @Override
    public void clearMap() {
        googleMap.clear();
    }

    @Override
    public void setMapView(TrackingData locationData, float zoomLevel) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(locationData.getLat(), locationData.getLon()), zoomLevel));
    }

    @Override
    public void setMapView(TrackingData locationData) {
        setMapView(locationData, defaultZoomLevel);
    }

    @Override
    public void setMapView(TrackingData[] locationDataList) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(getLatLngBoundsFromTrackingDataArray(locationDataList), 50));
    }

    private LatLngBounds getLatLngBoundsFromTrackingDataArray(TrackingData[] locationDataList) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (TrackingData locationData : locationDataList) {
            builder.include(new LatLng(locationData.getLat(), locationData.getLon()));
        }

        return builder.build();
    }

    @Override
    public void setZoomLevel(TrackingData[] locationDataList) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(getLatLngBoundsFromTrackingDataArray(locationDataList), 50));
    }

    @Override
    public void setZoomLevel(TrackingData[] locationDataList, int[] paddingLeftTopRightBottom) {
        googleMap.setPadding(paddingLeftTopRightBottom[0], paddingLeftTopRightBottom[1], paddingLeftTopRightBottom[2], paddingLeftTopRightBottom[3]);
        setZoomLevel(locationDataList);

    }

    @Override
    public void setZoomLevel(int zoomLevel) {
        this.defaultZoomLevel = zoomLevel;
    }

    @Override
    public GoogleMap getMap() {
        return googleMap;
    }

    @Override
    public int drawPath(PathDataManager pathDataHolder, PathProperties... pathProperties) {
        if(pathProperties.length > 0){
            routePathDrawer.setPathProperties(pathProperties[0]);
        }
        return routePathDrawer.drawPath(pathDataHolder);
    }

    @Override
    public void drawPath(List<TrackingData> trackingDataList, PathProperties... pathProperties) {
        if(pathProperties.length > 0){
            routePathDrawer.setPathProperties(pathProperties[0]);
        }
        routePathDrawer.drawPath(trackingDataList);
    }

    @Override
    public void clearPath(int pathId) {
        routePathDrawer.removeLine(pathId);
    }

    @Override
    public void clearAllPaths() {
        routePathDrawer.removeAllLines();
    }

    @Override
    public void setupMapMoveStartListener(ObserverHost moveStartObserverHost) {
        if(googleMap != null && mapMoveStartObserverHost == null){
            mapMoveStartObserverHost = moveStartObserverHost;
            moveStartObserverHost.addToObservers(new TypedObserverImpl() {
                @Override
                public void update(Object o) {
                    isMapMoving = true;
                }
            });
            googleMap.setOnCameraMoveStartedListener((GoogleMap.OnCameraMoveStartedListener)moveStartObserverHost);
        }
    }

    @Override
    public void setupDragListener(final ObserverHost cameraMoveObserver){
        if (googleMap != null && mapDragObserverHost == null) {
            mapDragObserverHost = cameraMoveObserver;
            googleMap.setOnCameraMoveListener((GoogleMap.OnCameraMoveListener)cameraMoveObserver);
        }
    }

    @Override
    public void setOnMarkerClickAction(final ObserverHost<MarkerInformation> clickObservable) {
        if (googleMap != null && mapMarkerClickObserverHost == null) {
            mapMarkerClickObserverHost = clickObservable;
            googleMap.setOnMarkerClickListener((GoogleMap.OnMarkerClickListener)clickObservable);
        }
    }

    @Override
    public void setOnMapClickAction(final ObserverHost<TrackingData> clickObservable) {
        if (googleMap != null && mapClickObserverHost == null) {
            this.mapClickObserverHost = clickObservable;
            googleMap.setOnMapClickListener((GoogleMap.OnMapClickListener)clickObservable);
        }
    }

    @Override
    public void setupMapIdleCenterListener(ObserverHost centerCoordinateFetcher) {
        if (googleMap != null && mapIdleObserverHost == null) {
            mapIdleObserverHost = centerCoordinateFetcher;
            centerCoordinateFetcher.addToObservers(new TypedObserverImpl() {
                @Override
                public void update(Object o) {
                    isMapMoving = false;
                }
            });
            googleMap.setOnCameraIdleListener((GoogleMap.OnCameraIdleListener) centerCoordinateFetcher);
        }
    }

    @Override
    public ObserverHost<TrackingData> getMapClickObserverHost() {
        return mapClickObserverHost;
    }

    @Override
    public ObserverHost getMapDragObserverHost() {
        return mapDragObserverHost;
    }

    @Override
    public ObserverHost<MarkerInformation> getMapMarkerClickObserverHost() {
        return mapMarkerClickObserverHost;
    }

    @Override
    public ObserverHost getMapIdleObserverHost() {
        return mapIdleObserverHost;
    }

    @Override
    public ObserverHost getMapMoveStartObserverHost() {
        return mapMoveStartObserverHost;
    }

    @Override
    public boolean isLocationInScreen(TrackingData trackingData) {
        return googleMap.getProjection().getVisibleRegion().latLngBounds.contains(new LatLng(trackingData.getLat(), trackingData.getLon())); //convert to xy Point
    }

    @Override
    public boolean isMapMoving() {
        return isMapMoving;
    }

    @Override
    public void enableDisableMapInteraction(boolean enableDisable) {
        googleMap.getUiSettings().setAllGesturesEnabled(enableDisable);
    }
}
