package com.thinknsync.mapslib.mapWorks.markers;

import com.thinknsync.mapslib.mapWorks.markers.markerDrawables.MarkerDrawable;
import com.thinknsync.positionmanager.TrackingData;

public interface MarkerInformation<T> {
    int getId();
    void setId(int id);
    String getTitle();
    void setTitle(String title);
    MarkerDrawable getSelectedDrawable();
    void setSelectedDrawable(MarkerDrawable selectedDrawable);
    MarkerDrawable getUnselectedDrawable();
    void setUnselectedDrawable(MarkerDrawable unselectedDrawable);
    MarkerDrawable getActiveDrawable();
    boolean isSelected();
    void setSelected(boolean selected);
    void setOnclickInfoDialog(MarketInfoDialog onclickInfoDialog);
    MarketInfoDialog getOnclickInfoDialog();
    void showMarkerDialog();
    void hideMarkerDialog();
    TrackingData getTrackingData();
    void setTrackingData(TrackingData trackingData);
    T getMarkerObject();
    void setMarkerObject(T markerObject);
    void adjustClickDialogPosition(int[] xyArray);
}
