package com.thinknsync.mapslib.mapWorks;

public interface OnMapsReady {
    void onMapsReady();
    void onMapsLoadComplete();
}
