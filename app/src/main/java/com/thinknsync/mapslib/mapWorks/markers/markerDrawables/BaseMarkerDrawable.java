package com.thinknsync.mapslib.mapWorks.markers.markerDrawables;

public abstract class BaseMarkerDrawable implements MarkerDrawable {
    protected double widthRatio = 1;
    protected double heightRatio = 1;

    @Override
    public void setWidthRatio(double widthRatio) {
        this.widthRatio = widthRatio;
    }

    @Override
    public void setHeightRatio(double heightPercentage) {
        this.heightRatio = heightPercentage;
    }

    protected int getIconWidth(int actualWidth) {
        return (int)(actualWidth * widthRatio);
    }

    protected int getIconHeight(int actualHeight) {
        return (int)(actualHeight * heightRatio);
    }
}
