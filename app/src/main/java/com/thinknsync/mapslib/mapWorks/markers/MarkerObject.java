package com.thinknsync.mapslib.mapWorks.markers;

import com.thinknsync.mapslib.mapWorks.markers.markerDrawables.MarkerDrawable;
import com.thinknsync.positionmanager.TrackingData;

public class MarkerObject<T> implements MarkerInformation<T> {
    private int id = -1;
    private String title;
    private MarkerDrawable selectedDrawable;
    private MarkerDrawable unselectedDrawable;
    private boolean isSelected;
    private MarketInfoDialog onclickInfoDialog;
    private TrackingData trackingData;
    private T markerObject;

    public MarkerObject(TrackingData trackingData){
        this.trackingData = trackingData;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public MarkerDrawable getSelectedDrawable() {
        return selectedDrawable;
    }

    @Override
    public void setSelectedDrawable(MarkerDrawable selectedDrawable) {
        this.selectedDrawable = selectedDrawable;
    }

    @Override
    public MarkerDrawable getUnselectedDrawable() {
        return unselectedDrawable;
    }

    @Override
    public void setUnselectedDrawable(MarkerDrawable unselectedDrawable) {
        this.unselectedDrawable = unselectedDrawable;
    }

    @Override
    public MarkerDrawable getActiveDrawable(){
        if(isSelected()){
            return selectedDrawable;
        }
        return unselectedDrawable;
    }

    @Override
    public boolean isSelected() {
        return isSelected;
    }

    @Override
    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public void setOnclickInfoDialog(MarketInfoDialog onclickInfoDialog) {
        this.onclickInfoDialog = onclickInfoDialog;
    }

    @Override
    public MarketInfoDialog getOnclickInfoDialog() {
        return onclickInfoDialog;
    }

    @Override
    public void showMarkerDialog() {
        if(onclickInfoDialog != null) {
            onclickInfoDialog.showDialog();
        }
    }

    @Override
    public void hideMarkerDialog() {
        if(onclickInfoDialog != null) {
            onclickInfoDialog.hideDialog();
        }
    }

    @Override
    public TrackingData getTrackingData() {
        return trackingData;
    }

    @Override
    public void setTrackingData(TrackingData trackingData) {
        this.trackingData = trackingData;
    }

    @Override
    public T getMarkerObject() {
        return markerObject;
    }

    @Override
    public void setMarkerObject(T markerObject) {
        this.markerObject = markerObject;
    }

    @Override
    public void adjustClickDialogPosition(int[] xyArray) {
        if(onclickInfoDialog != null){
            onclickInfoDialog.adjustDialogPosition(xyArray);
        }
    }
}
