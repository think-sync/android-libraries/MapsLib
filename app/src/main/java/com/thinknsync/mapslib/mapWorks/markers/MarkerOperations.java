package com.thinknsync.mapslib.mapWorks.markers;

import com.thinknsync.positionmanager.TrackingData;

import java.util.List;

public interface MarkerOperations<T> {
    String tag = "markerOperations";

    void setPin(MarkerInformation markerInformation);

    void forceUpdatePin(TrackingData locationData);

    MarkerInformation getMarkerInformationById(int markerId);

    void removeMarkerIfExists(int markerId);

    void removeMarkerIfExists(MarkerInformation markerInformation);

    void removeAllMarkers();

    void removeAllMarkersExcept(int markerId);

    void removeAllMarkersExcept(MarkerInformation markerInformation);

    void setMapObject(T mapObject);

    boolean doesMarkerExist(int markerId);

    boolean doesMarkerExist(MarkerInformation markerInformation);

    void onMarkerSelected(MarkerInformation markerInformation);

    void onMarkerUnSelected(MarkerInformation markerInformation);

    void swapMarkerSelection(MarkerInformation markerInformation);

    void swapMarkerSelection(int markerId);

    void unSelectAllMarkers();

    void onMarkerSelected(int integer);

    void onMarkerUnselected(int  integer);

    int[] getMarkerPositionXyArray(MarkerInformation markerInformation);

    int[] getMarkerPositionXyArray(int markerId);

    List<MarkerInformation> getAllMarkers();
}
