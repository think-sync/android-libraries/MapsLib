package com.thinknsync.mapslib.mapWorks.mapListeners;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.thinknsync.mapslib.MapsOperation;
import com.thinknsync.mapslib.mapWorks.markers.MarkerInformation;
import com.thinknsync.mapslib.mapWorks.markers.MarkerOperations;
import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.observerhost.TypedObserverImpl;

public class DefaultMarkerClickListener extends ObserverHostImpl<MarkerInformation> implements GoogleMap.OnMarkerClickListener {
    private MarkerOperations markerOperations;
    private MapsOperation mapsOperation;

    public DefaultMarkerClickListener(MarkerOperations markerOperations, MapsOperation mapsOperation) {
        this.markerOperations = markerOperations;
        this.mapsOperation = mapsOperation;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        final int markerId = (int)marker.getTag();
        notifyObservers(markerOperations.getMarkerInformationById(markerId));
//        mapsOperation.getMapIdleObserverHost().addToObservers(new TypedObserverImpl<Object>() {
//            @Override
//            public void update(Object o) {
//            }
//
//            @Override
//            public void update(ObserverHost<Object> observerHost, Object o) {
//                observerHost.removeObserver(this);
//            }
//        });
        markerOperations.swapMarkerSelection(markerId);
        return true;
    }
}
