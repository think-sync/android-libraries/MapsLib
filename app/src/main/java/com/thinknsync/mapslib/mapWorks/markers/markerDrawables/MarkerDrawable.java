package com.thinknsync.mapslib.mapWorks.markers.markerDrawables;

import com.thinknsync.objectwrappers.FrameworkWrapper;

public interface MarkerDrawable {
    Type getDrawableType();
    void setWidthRatio(double widthRatio);
    void setHeightRatio(double heightPercentage);
    FrameworkWrapper getDrawableForMarker();

    enum Type {
        VIEW, XML, DRAWABLE;
    }
}
