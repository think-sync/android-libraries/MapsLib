package com.thinknsync.mapslib.mapWorks;

import com.thinknsync.mapslib.MapsOperation;
import com.thinknsync.mapslib.place.PlaceDataObject;
import com.thinknsync.positionmanager.TrackingData;

public interface MapsUiInterface {
    interface View extends OnMapsReady {
        String tag = "mapsUiView";

        void initMaps();

        void updateCameraIdlePlace(PlaceDataObject place);

        void onLocationReceived(TrackingData locationData);

        MapsOperation getMapsOperationUi();
    }

    interface Controller<T extends View>{
        void initMapsManager();
    }
}
