package com.thinknsync.mapslib.mapWorks.mapListeners;

import android.os.Handler;

import com.thinknsync.mapslib.MapsOperation;
import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.positionmanager.TrackingData;

/**
 * Created by shuaib on 5/19/17.
 */

public class InitMapLocationObserver extends TypedObserverImpl<TrackingData> {

    private MapsOperation mapsOperation;

    public InitMapLocationObserver(MapsOperation mapsManager){
        this.mapsOperation = mapsManager;
    }

    @Override
    public void update(TrackingData trackingData) {

    }

    @Override
    public void update(final ObserverHost host, TrackingData locationData) {
        mapsOperation.setMapView(locationData);
//        mapsOperation.setPin(locationData, PinType.DEFAUT, true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                host.removeObserver(InitMapLocationObserver.this);
            }
        }, 500);
    }

}
