package com.thinknsync.mapslib;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.positionmanager.TrackingData;

public final class Utils {
    public static String getGoogleApiKey(FrameworkWrapper<Context> contextWrapper) {
        String value = "";
        try {
            Context context = contextWrapper.getFrameworkObject();
            ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            value = (String) ai.metaData.get("com.google.android.geo.API_KEY");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return value;
    }

    public static double distanceBetweenInMeters(TrackingData locationData1, TrackingData locationData2) {
        double d2r = Math.PI / 180;

        double dlong = (locationData1.getLon() - locationData2.getLon()) * d2r;
        double dlat = (locationData1.getLat() - locationData2.getLat()) * d2r;
        double a = Math.pow(Math.sin(dlat / 2.0), 2) + Math.cos(locationData2.getLat() * d2r)
                * Math.cos(locationData1.getLat() * d2r) * Math.pow(Math.sin(dlong / 2.0), 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = 6367 * c;
        return d * 1000;
    }
}
