package com.thinknsync.mapslib.apiCall;

import com.thinknsync.apilib.ApiDataObject;
import com.thinknsync.apilib.ApiRequestImpl;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.objectwrappers.AndroidContextWrapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by shuaib on 5/20/17.
 */

public class GoogelGeoCodeWaypointApiRequestImpl extends ApiRequestImpl {

    public GoogelGeoCodeWaypointApiRequestImpl(AndroidContextWrapper c, String url, ApiDataObject dataObject, int requestMethod,
                                               ApiResponseActions volleyListenerAction, int reqid) {
        super(c, url, dataObject, requestMethod, volleyListenerAction, reqid);
    }

    @Override
    protected void callResponseAction(JSONObject response){
        try {
            if (response.getString(ApiCallFields.GoogleApiResponseFields.KEY_STATUS).equalsIgnoreCase(ApiCallFields.GoogleApiResponseFields.VALUE_STATUS_OK)) {
                volleyListenerAction.onApiCallSuccess(reqId, response);
                return;
            } else if(response.getString(ApiCallFields.GoogleApiResponseFields.KEY_STATUS).equalsIgnoreCase(ApiCallFields.GoogleApiResponseFields.VALUE_NO_RESULT)){
                response.put(ApiFields.KEY_MESSAGE, "No results found");
            }

            volleyListenerAction.onApiCallFailure(reqId, response);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected JSONObject getRequestObject() {
        return new JSONObject((Map)this.dataObject.getObject());
    }
}
