package com.thinknsync.mapslib.apiCall;

public interface ApiCallFields {
    interface GoogleApiResponseFields {
        String KEY_STATUS = "status";
        String VALUE_STATUS_OK = "ok";
        String VALUE_NO_RESULT = "zero_results";
        String VALUE_OVER_LIMIT = "OVER_QUERY_LIMIT";
    }
}
