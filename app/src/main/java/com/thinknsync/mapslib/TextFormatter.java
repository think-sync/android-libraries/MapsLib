package com.thinknsync.mapslib;

import java.text.DecimalFormat;

/**
 * Created by shuaib on 6/13/17.
 */

public final class TextFormatter {
    public String get2DigitDecimal(double digit) {
        DecimalFormat df2 = new DecimalFormat(".##");
        return df2.format(digit);
    }

    public DecimalFormat getNumberFormatter1DecimalDigit() {
        return new DecimalFormat("##.#");
    }
}