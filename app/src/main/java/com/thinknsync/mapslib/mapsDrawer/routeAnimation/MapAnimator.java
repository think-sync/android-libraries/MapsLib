package com.thinknsync.mapslib.mapsDrawer.routeAnimation;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.thinknsync.mapslib.mapsDrawer.PathDrawer;

import java.util.List;

/**
 * Created by amal.chandran on 22/12/16.
 */

public class MapAnimator {

    private static MapAnimator mapAnimator;

    private Polyline backgroundPolyline;

    private Polyline foregroundPolyline;

    private PolylineOptions optionsForeground;

    private AnimatorSet firstRunAnimSet;

    private AnimatorSet secondLoopRunAnimSet;

    //    static final int BACKGROUND_COLOR = Color.parseColor("#FFA7A6A6");
    int BACKGROUND_COLOR;
    int FOREGROUND_COLOR = PathDrawer.LineColor;
    int LINE_WIDTH = PathDrawer.LineWidth;
    final float lighteningFactor = 0.8f;


    private MapAnimator() {
        BACKGROUND_COLOR = lighterColor(FOREGROUND_COLOR, lighteningFactor);
    }

    public static MapAnimator getInstance() {
        if (mapAnimator == null) mapAnimator = new MapAnimator();
        return mapAnimator;
    }

    public void setLineColor(int foregroundColor) {
        this.FOREGROUND_COLOR = foregroundColor;
        this.BACKGROUND_COLOR = lighterColor(foregroundColor, lighteningFactor);
    }

    public void setLineWidth(int lineWidth) {
        this.LINE_WIDTH = lineWidth;
    }

    private int lighterColor(int color, float factor) {
        int a = Color.alpha(color);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);

        return Color.argb(a * (int)(factor * 255), r, g, b);
    }

    public Polyline[] animateRoute(GoogleMap googleMap, List<LatLng> coordinateList) {
        if (firstRunAnimSet == null) {
            firstRunAnimSet = new AnimatorSet();
        } else {
            firstRunAnimSet.removeAllListeners();
            firstRunAnimSet.end();
            firstRunAnimSet.cancel();

            firstRunAnimSet = new AnimatorSet();
        }
        if (secondLoopRunAnimSet == null) {
            secondLoopRunAnimSet = new AnimatorSet();
        } else {
            secondLoopRunAnimSet.removeAllListeners();
            secondLoopRunAnimSet.end();
            secondLoopRunAnimSet.cancel();

            secondLoopRunAnimSet = new AnimatorSet();
        }
        //Reset the polylines
        if (foregroundPolyline != null) foregroundPolyline.remove();
        if (backgroundPolyline != null) backgroundPolyline.remove();


        PolylineOptions optionsBackground = new PolylineOptions().add(coordinateList.get(0)).color(BACKGROUND_COLOR).width(LINE_WIDTH);
        backgroundPolyline = googleMap.addPolyline(optionsBackground);

        optionsForeground = new PolylineOptions().add(coordinateList.get(0)).color(FOREGROUND_COLOR).width(LINE_WIDTH);
        foregroundPolyline = googleMap.addPolyline(optionsForeground);

        final ValueAnimator percentageCompletion = ValueAnimator.ofInt(0, 100);
        percentageCompletion.setDuration(1200);
        percentageCompletion.setInterpolator(new DecelerateInterpolator());
        percentageCompletion.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                List<LatLng> foregroundPoints = backgroundPolyline.getPoints();

                int percentageValue = (int) animation.getAnimatedValue();
                int pointcount = foregroundPoints.size();
                int countTobeRemoved = (int) (pointcount * (percentageValue / 100.0f));
                List<LatLng> subListTobeRemoved = foregroundPoints.subList(0, countTobeRemoved);
                subListTobeRemoved.clear();

                foregroundPolyline.setPoints(foregroundPoints);
            }
        });
        percentageCompletion.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                foregroundPolyline.setColor(BACKGROUND_COLOR);
                foregroundPolyline.setPoints(backgroundPolyline.getPoints());
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });


        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), BACKGROUND_COLOR, FOREGROUND_COLOR);
        colorAnimation.setInterpolator(new AccelerateInterpolator());
        colorAnimation.setDuration(500); // milliseconds

        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                foregroundPolyline.setColor((int) animator.getAnimatedValue());
            }

        });


        secondLoopRunAnimSet.playSequentially(colorAnimation,
                percentageCompletion);
        secondLoopRunAnimSet.setStartDelay(500);

        secondLoopRunAnimSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                secondLoopRunAnimSet.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        PolylineOptions options = new PolylineOptions().width(LINE_WIDTH).color(BACKGROUND_COLOR).geodesic(true);
        for (int z = 0; z < coordinateList.size(); z++) {
            LatLng point = coordinateList.get(z);
            options.add(point);
        }
        Polyline line = googleMap.addPolyline(options);

        backgroundPolyline.setPoints(line.getPoints());
        secondLoopRunAnimSet.start();
        return new Polyline[]{line, backgroundPolyline, foregroundPolyline};
    }
}

