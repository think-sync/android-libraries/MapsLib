package com.thinknsync.mapslib.mapsDrawer;

import com.thinknsync.mapslib.mapWorks.markers.MarkerInformation;

public class PathProperties {

    private int lineColor = PathDrawer.LineColor;
    private int lineWidth = PathDrawer.LineWidth;
    private MarkerInformation<?> startPointMarker = null;
    private MarkerInformation<?> endPointMarker = null;

    public int getLineColor() {
        return lineColor;
    }

    public void setLineColor(int lineColor) {
        this.lineColor = lineColor;
    }

    public int getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(int lineWidth) {
        this.lineWidth = lineWidth;
    }

    public MarkerInformation<?> getStartPointMarker() {
        return startPointMarker;
    }

    public void setStartPointMarker(MarkerInformation<?> startPointMarker) {
        this.startPointMarker = startPointMarker;
    }

    public MarkerInformation<?> getEndPointMarker() {
        return endPointMarker;
    }

    public void setEndPointMarker(MarkerInformation<?> endPointMarker) {
        this.endPointMarker = endPointMarker;
    }
}
