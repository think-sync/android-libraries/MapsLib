package com.thinknsync.mapslib.mapsDrawer;

import com.thinknsync.mapslib.Units;
import com.thinknsync.positionmanager.TrackingData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by shuaib on 5/25/17.
 */

public interface PathDataManager {

    String key_routes = "routes";
    String key_legs = "legs";
    String key_distance = "distance";
    String key_duration = "duration";
    String key_value = "value";

    JSONObject getPathData();

    void setPathData(JSONObject pathData);

    double getTotalDistance();

    Units getUnitDistance();

    void setUnitDistance(Units unitDistance);

    double getTotalTime();

    TimeUnit getUnitTime();

    void setUnitTime(TimeUnit unitTime);

    String getDistanceString();

    String getTimeString();

    List<TrackingData> getPathCoordinates();

    double getPathDistanceInMeters() throws JSONException;

    int getCommuteTimeInSecs() throws JSONException;
}
