package com.thinknsync.mapslib.mapsDrawer;

import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.mapslib.Units;
import com.thinknsync.mapslib.apiCall.ApiCallFields;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.positionmanager.TrackingData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

public class GoogleMapsDistanceCalculator extends ObserverHostImpl<Double> {
    private final AndroidContextWrapper contextWrapper;
    private final PathDataFetcher.NavigationMode navigationMode;
    private final Units distanceUnit;

    public GoogleMapsDistanceCalculator(AndroidContextWrapper contextWrapper, Units distanceUnit, PathDataFetcher.NavigationMode navigationMode) {
        this.contextWrapper = contextWrapper;
        this.navigationMode = navigationMode;
        this.distanceUnit = distanceUnit;
    }

    public void getTraveledDistance(TrackingData coordinate1, TrackingData coordinate2) {
        PathDataFetcher dataFetcher = new GoogleMapsPathDataGetter(contextWrapper, new TrackingData[]{coordinate1, coordinate2}, navigationMode);
        dataFetcher.getRouteData(new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException {
                PathDataManager pathData = new PathDataManagerImpl(responseJson, distanceUnit, TimeUnit.MINUTES);
                notifyObservers(pathData.getPathDistanceInMeters());
            }

            @Override
            public void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException {
                if(responseJson.getString(ApiCallFields.GoogleApiResponseFields.KEY_STATUS).equalsIgnoreCase(ApiCallFields.GoogleApiResponseFields.VALUE_OVER_LIMIT)){
                    notifyError(new Exception("could not fetch path data from google maps"));
                }
            }
        });
    }
}
