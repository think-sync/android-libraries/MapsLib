package com.thinknsync.mapslib.mapsDrawer;

import android.content.Context;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.thinknsync.mapslib.mapWorks.markers.MarkerInformation;
import com.thinknsync.mapslib.mapsDrawer.routeAnimation.MapAnimator;
import com.thinknsync.objectwrappers.BitmapWrapper;
import com.thinknsync.positionmanager.TrackingData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by shuaib on 5/20/17.
 */

public class GoogleMapsPathDrawer implements PathDrawer {

    private GoogleMap googleMap;
    private PathProperties pathProperties;
    private Context context;
    private HashMap<Integer, Polyline[]> linesDrawn;

    public GoogleMapsPathDrawer(GoogleMap googleMap, Context context) {
        this.googleMap = googleMap;
//        logger = CustomLog.getInstance();
        this.context = context;
        pathProperties = new PathProperties();
        linesDrawn = new HashMap<>();
    }

    @Override
    public void setPathProperties(PathProperties pathProperties) {
        this.pathProperties = pathProperties;
    }

    @Override
    public List<Integer> getLinesDrawn() {
        return new ArrayList<>(linesDrawn.keySet());
    }

    @Override
    public int getLastLineDrawn() {
        List<Integer> drawnLineNumbers = getLinesDrawn();
        return drawnLineNumbers.get(drawnLineNumbers.size() - 1);
    }

    @Override
    public void removeLine(int polylineId) {
        if(linesDrawn.get(polylineId) != null){
            for (Polyline polyline : linesDrawn.get(polylineId)) {
                polyline.remove();
            }
            linesDrawn.remove(polylineId);
        }
    }

    @Override
    public void removeAllLines() {
        for (Polyline[] polylines : linesDrawn.values()) {
            for (Polyline polyline : polylines) {
                polyline.remove();
            }
        }
        linesDrawn = new HashMap<>();
    }

    @Override
    public int drawPath(PathDataManager pathData) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        List<LatLng> points = convertLocationDataToLatLng(pathData.getPathCoordinates());

        for (LatLng latLng : points) {
            builder.include(latLng);
        }

        if (points.size() > 0) {
            final List<LatLng> finalPoints = points;
            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 0));
            MapAnimator mapAnimator = MapAnimator.getInstance();
            mapAnimator.setLineColor(pathProperties.getLineColor());
            mapAnimator.setLineWidth(pathProperties.getLineWidth());
            Polyline[] lineDrawn = mapAnimator.animateRoute(googleMap, finalPoints);
            linesDrawn.put(linesDrawn.size(), lineDrawn);

            if(pathProperties.getStartPointMarker() != null){
                googleMap.addMarker(new MarkerOptions().draggable(false).position(finalPoints.get(0))
                        .icon(getBitmapDescriptorFromMarker(pathProperties.getStartPointMarker())));
            }

            if(pathProperties.getEndPointMarker() != null){
                googleMap.addMarker(new MarkerOptions().draggable(false).position(finalPoints.get(0))
                        .icon(getBitmapDescriptorFromMarker(pathProperties.getEndPointMarker())));
            }
        }
        return getLastLineDrawn();
    }

    @Override
    public void drawPath(List<TrackingData> pathData) {
        List<LatLng> latLngList = new ArrayList<>();
        PolylineOptions lineOptions = new PolylineOptions();
        for (TrackingData trackingData : pathData) {
            latLngList.add(new LatLng(trackingData.getLat(), trackingData.getLon()));
        }
        lineOptions.addAll(latLngList);
        googleMap.addPolyline(lineOptions);
    }

    private BitmapDescriptor getBitmapDescriptorFromMarker(MarkerInformation<?> marker){
        return BitmapDescriptorFactory.fromBitmap(((BitmapWrapper)marker
                .getActiveDrawable().getDrawableForMarker()).getFrameworkObject());
    }

    private List<LatLng> convertLocationDataToLatLng(List<TrackingData> locationDataList){
        List<LatLng> latLngList = new ArrayList<>();
        for (TrackingData locationData : locationDataList){
            latLngList.add(new LatLng(locationData.getLat(), locationData.getLon()));
        }

        return latLngList;
    }
}
